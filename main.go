package main

import (
	"fmt"
)

func main() {
	// Testing generic nodes
	n := NewFloat64Node(3.2)
	n2 := NewFloat64Node(4.5)
	n3 := NewFloat64Node(3.5)

	n.next = &n2
	n2.next = &n3

	fmt.Println(n.hasUniqueElements())

	n3.data = 3.2

	fmt.Println(n.hasUniqueElements())

	// Testing generic queue

	q := NewStringQueue()
	q.Enqueue("Hello")
	q.Enqueue("World")

	value := q.Dequeue()
	if value.err == nil {
		fmt.Println(value.value)
	} else {
		fmt.Println(value.err.Error())
	}

	value = q.Dequeue()
	if value.err == nil {
		fmt.Println(value.value)
	} else {
		fmt.Println(value.err.Error())
	}

	value = q.Dequeue()
	if value.err != nil {
		fmt.Println(value.err.Error())
	}

	fmt.Println("Hello world!")
}
