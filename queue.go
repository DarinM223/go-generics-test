package main

import (
	"errors"
	_ "fmt"
)

//go:generate genny -in=$GOFILE -out=gen-$GOFILE gen "Something=string,int,float64"
type SomethingQueue struct {
	first *SomethingNode
	last  *SomethingNode
}

func NewSomethingQueue() SomethingQueue {
	return SomethingQueue{
		first: nil,
		last:  nil,
	}
}

func (s *SomethingQueue) Enqueue(data Something) {
	newNode := NewSomethingNode(data)
	if s.first == nil { // set first element
		s.first = &newNode
		s.last = s.first
	} else {
		s.last.next = &newNode
		s.last = s.last.next
	}
}

type SomethingQueueRetType struct {
	value Something
	err   error
}

func (s *SomethingQueue) Dequeue() SomethingQueueRetType {
	if s.first == nil {
		return SomethingQueueRetType{
			err: errors.New("The queue is empty"),
		}
	} else {
		value := s.first.data
		s.first = s.first.next

		return SomethingQueueRetType{
			value: value,
			err:   nil,
		}
	}
}
