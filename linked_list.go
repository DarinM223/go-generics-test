package main

import (
	"github.com/cheekybits/genny/generic"
)

type Something generic.Type

//go:generate genny -in=$GOFILE -out=gen-$GOFILE gen "Something=string,int,float64"
type SomethingNode struct {
	data Something
	next *SomethingNode
}

func NewSomethingNode(data Something) SomethingNode {
	return SomethingNode{
		data: data,
		next: nil,
	}
}

func (s *SomethingNode) hasUniqueElements() bool {
	dict := make(map[Something]bool)
	counter := s
	for counter != nil {
		_, ok := dict[counter.data]
		if ok {
			return false
		} else {
			dict[counter.data] = true
		}
		counter = counter.next
	}
	return true
}
